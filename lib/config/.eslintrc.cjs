module.exports = {
	env: {
		es2021: true,
	},
	plugins: [
		'@typescript-eslint',
		'no-use-extend-native',
		'sonarjs',
		'unicorn',
		'promise',
		'import',
		'n',
		'eslint-comments'
	],
	extends: [
		'eslint:recommended',
		'plugin:@typescript-eslint/recommended-type-checked',
		'plugin:@typescript-eslint/stylistic-type-checked',
		'plugin:no-use-extend-native/recommended',
		'plugin:sonarjs/recommended',
		'plugin:unicorn/recommended',
		'plugin:promise/recommended',
		'plugin:import/recommended',
		'plugin:import/typescript',
		'plugin:n/recommended-module',
		'plugin:eslint-comments/recommended',
		'prettier',
	],
	parser: '@typescript-eslint/parser',
	parserOptions: {
		ecmaVersion: 'latest',
		sourceType: 'module',
	},
	overrides: [
		{
			env: {
				node: true
			},
			files: [
				'.eslintrc.{js,cjs}'
			],
			parserOptions: {
				sourceType: 'script'
			},
		},
	],
	rules: {
		'unicorn/prevent-abbreviations': [
			'error',
			{
				checkFilenames: false,
				checkDefaultAndNamespaceImports: false,
				checkShorthandImports: false,
				extendDefaultReplacements: false,
				replacements: {
					whitelist: { include: true },
					blacklist: { exclude: true },
					master: { main: true },
					slave: { secondary: true },
					application: { app: true },
					applications: { apps: true },
					arr: { array: true },
					e: { error: true, event: true },
					el: { element: true },
					elem: { element: true },
					len: { length: true },
					msg: { message: true },
					num: { number: true },
					obj: { object: true },
					opts: { options: true },
					param: { parameter: true },
					params: { parameters: true },
					prev: { previous: true },
					req: { request: true },
					res: { response: true, result: true },
					ret: { returnValue: true },
					str: { string: true },
					temp: { temporary: true },
					tmp: { temporary: true },
					val: { value: true },
					err: { error: true },
				},
			},
		],
	},
}
