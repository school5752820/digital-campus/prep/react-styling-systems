export const App = () => {
	return (
		<main className="w-full max-w-lg mx-auto bg-slate-100 rounded-md p-4">
			<img
				className="w-full aspect-video object-cover object-center rounded"
				src="https://source.unsplash.com/random/800x600"
				alt=""
			/>
			<h1 className="text-3xl my-6 font-bold">
				Lorem ipsum dolor sit amet consectetur adipisicing elit.
			</h1>
			<p className="text-slate-900">
				Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusantium quaerat vitae
				voluptatibus ad iste neque beatae officia architecto, corporis expedita placeat, quas
				consequatur blanditiis. Atque voluptas ad cupiditate? Repellendus, cum.
			</p>
			<a
				href="#!"
				className="block text-center font-bold uppercase no-underline px-6 py-4 rounded bg-slate-900 text-slate-100 mt-6"
			>
				Lire la suite
			</a>
		</main>
	);
};
