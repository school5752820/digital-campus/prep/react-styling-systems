const baseConfig = require('@conduit/config/eslint');

/** @type {import('eslint').ESLint.ConfigData} */
module.exports = {
	...baseConfig,
	plugins: [...baseConfig.plugins, 'react-refresh'],
	extends: [...baseConfig.extends, 'plugin:react-hooks/recommended'],
	env: { browser: true, es2020: true },
	ignorePatterns: ['dist', '.eslintrc.cjs'],
	parserOptions: {
		...baseConfig.parserOptions,
		tsconfigRootDir: __dirname,
		project: ['../../lib/config/tsconfig.eslint.json', './tsconfig.node.json', './tsconfig.json'],
	},
	rules: {
		'react-refresh/only-export-components': ['warn', { allowConstantExport: true }],
		'unicorn/prevent-abbreviations': 'off',
		'@typescript-eslint/no-empty-interface': ['warn', { allowSingleExtends: true }],
	},
};
