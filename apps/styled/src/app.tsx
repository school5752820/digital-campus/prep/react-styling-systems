import styled from '@emotion/styled';

const Card = styled.main`
	inline-size: 100%;
	max-inline-size: 500px;
	margin-inline: auto;
	background-color: #f8fafc;
	border-radius: 8px;
	padding: 16px;
`;

const Image = styled.img`
	inline-size: 100%;
	block-size: auto;
	aspect-ratio: 16/9;
	object-fit: cover;
	object-position: center;
	border-radius: 4px;
`;

const Title = styled.h1`
	line-height: 1.2;
	margin-block: 24px;
`;

const Description = styled.p`
	color: #0f172a;
`;

const Link = styled.a`
	display: block;
	text-align: center;
	font-weight: 700;
	text-transform: uppercase;
	text-decoration: none;
	padding: 16px 24px;
	border-radius: 4px;
	background-color: #0f172a;
	color: #f8fafc;
	margin-block-start: 24px;
`;

export const App = () => {
	return (
		<Card>
			<Image src="https://source.unsplash.com/random/800x600" alt="" />
			<Title>Lorem ipsum dolor sit amet consectetur adipisicing elit.</Title>
			<Description>
				Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusantium quaerat vitae
				voluptatibus ad iste neque beatae officia architecto, corporis expedita placeat, quas
				consequatur blanditiis. Atque voluptas ad cupiditate? Repellendus, cum.
			</Description>
			<Link href="#!">Lire la suite</Link>
		</Card>
	);
};
