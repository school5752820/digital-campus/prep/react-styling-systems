import { css } from '../styled-system/css';

const styles = {
	main: css({
		inlineSize: 'full',
		maxInlineSize: 500,
		mx: 'auto',
		bg: 'slate.100',
		rounded: 'md',
		p: '4',
	}),
	image: css({
		inlineSize: 'full',
		blockSize: 'auto',
		aspectRatio: '16 / 9',
		objectFit: 'cover',
		objectPosition: 'center',
		rounded: 'sm',
	}),
	title: css({
		fontSize: '3xl',
		fontWeight: 700,
		lineHeight: 1.2,
		my: '6',
	}),
	description: css({
		color: 'slate.900',
	}),
	link: css({
		display: 'block',
		textAlign: 'center',
		fontWeight: 700,
		textTransform: 'uppercase',
		textDecoration: 'none',
		px: '6',
		py: '4',
		rounded: 'md',
		bg: 'slate.900',
		color: 'slate.100',
		mt: '6',
	}),
} as const;

export const App = () => {
	return (
		<main className={styles.main}>
			<img className={styles.image} src="https://source.unsplash.com/random/800x600" alt="" />
			<h1 className={styles.title}>Lorem ipsum dolor sit amet consectetur adipisicing elit.</h1>
			<p className={styles.description}>
				Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusantium quaerat vitae
				voluptatibus ad iste neque beatae officia architecto, corporis expedita placeat, quas
				consequatur blanditiis. Atque voluptas ad cupiditate? Repellendus, cum.
			</p>
			<a href="#!" className={styles.link}>
				Lire la suite
			</a>
		</main>
	);
};
