import styles from './app.module.css';

export const App = () => {
	return (
		<main className={styles.card}>
			<img className={styles.image} src="https://source.unsplash.com/random/800x600" alt="" />
			<h1 className={styles.title}>Lorem ipsum dolor sit amet consectetur adipisicing elit.</h1>
			<p className={styles.description}>
				Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusantium quaerat vitae
				voluptatibus ad iste neque beatae officia architecto, corporis expedita placeat, quas
				consequatur blanditiis. Atque voluptas ad cupiditate? Repellendus, cum.
			</p>
			<a href="#!" className={styles.link}>
				Lire la suite
			</a>
		</main>
	);
};
